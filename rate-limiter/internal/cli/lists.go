package cli

import (
	"fmt"
	"strconv"

	"github.com/spf13/cobra"

	"rate-limiter/internal/grpc/limiter"
)

var (
	whitelistsCmd = &cobra.Command{
		Use:          "whitelists",
		Short:        "whitelists is a subcommand for managing whitelists",
		Run:          nil,
		SilenceUsage: true,
	}

	blacklistsCmd = &cobra.Command{
		Use:          "blacklists",
		Short:        "blacklists is a subcommand for managing blacklists",
		Run:          nil,
		SilenceUsage: true,
	}
)

func getAddCommand(run func(*cobra.Command, []string) error) *cobra.Command {
	return &cobra.Command{
		Use:   "add",
		Short: "add is a subcommand for adding subnet in list",
		RunE:  run,
		Args:  cobra.ExactArgs(2),
	}
}

func getDeleteCommand(run func(*cobra.Command, []string) error) *cobra.Command {
	return &cobra.Command{
		Use:   "delete",
		Short: "delete is a subcommand for removing subnet from list",
		RunE:  run,
		Args:  cobra.ExactArgs(2),
	}
}

// add subnet into blacklist
func (c *Cli) runAddToBlacklistE(cmd *cobra.Command, args []string) error {
	mask, err := strconv.Atoi(args[1])
	if err != nil {
		return err
	}

	_, err = c.client.AddSubNetInBlackList(c.ctx, &limiter.UpdateSubNetsListRequest{
		Ip:   args[0],
		Mask: uint32(mask),
	})
	if err != nil {
		return err
	}

	fmt.Println("Ok")

	return nil
}

func (c *Cli) runRemoveFromBlacklistE(cmd *cobra.Command, args []string) error {
	mask, err := strconv.Atoi(args[1])
	if err != nil {
		return err
	}

	_, err = c.client.RemoveSubNetFromBlackList(c.ctx, &limiter.UpdateSubNetsListRequest{
		Ip:   args[0],
		Mask: uint32(mask),
	})
	if err != nil {
		return err
	}

	fmt.Println("Ok")

	return nil
}

// add subnet into blacklist
func (c *Cli) runAddToWhitelistE(cmd *cobra.Command, args []string) error {
	mask, err := strconv.Atoi(args[1])
	if err != nil {
		return err
	}

	_, err = c.client.AddSubNetInWhiteList(c.ctx, &limiter.UpdateSubNetsListRequest{
		Ip:   args[0],
		Mask: uint32(mask),
	})
	if err != nil {
		return err
	}

	fmt.Println("Ok")

	return nil
}

func (c *Cli) runRemoveFromWhitelistE(cmd *cobra.Command, args []string) error {
	mask, err := strconv.Atoi(args[1])
	if err != nil {
		return err
	}

	_, err = c.client.RemoveSubNetFromWhiteList(c.ctx, &limiter.UpdateSubNetsListRequest{
		Ip:   args[0],
		Mask: uint32(mask),
	})
	if err != nil {
		return err
	}

	fmt.Println("Ok")

	return nil
}
