package main

import (
	"rate-limiter/internal/cli"
)

func main() {
	cli.Execute()
}
