package lists

import (
	"net"
	"sync"
	"testing"
)

func TestList_Add(t *testing.T) {
	type fields struct {
		subNets []*net.IPNet
	}

	type args struct {
		subnet *net.IPNet
	}

	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name:   "empty list",
			fields: fields{subNets: nil},
			args: args{
				subnet: &net.IPNet{
					IP:   net.IPv4(192, 168, 0, 1),
					Mask: net.IPMask{24},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := &List{subNets: tt.fields.subNets}
			l.Add(tt.args.subnet)
			if len(l.subNets) != 1 {
				t.Errorf("want 1 got %d", len(l.subNets))
			}
		})
	}
}

func TestList_Clear(t *testing.T) {
	type fields struct {
		subNets []*net.IPNet
	}

	tests := []struct {
		name   string
		fields fields
	}{
		{
			name: "clear list",
			fields: fields{
				subNets: []*net.IPNet{{
					IP:   net.IPv4(192, 168, 0, 1),
					Mask: net.IPMask{24},
				}},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := &List{
				subNets: tt.fields.subNets,
			}
			l.Clear()
			if len(l.subNets) != 0 {
				t.Errorf("want empty slice after Clear call")
			}
		})
	}
}

func TestList_Contains(t *testing.T) {
	type fields struct {
		subNets []*net.IPNet
	}

	type args struct {
		ip net.IP
	}

	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			"search in empty list",
			fields{
				subNets: nil,
			},
			args{
				ip: net.IPv4(192, 168, 0, 1),
			},
			false,
		},
		{
			"search ip presented in list",
			fields{
				subNets: []*net.IPNet{{
					IP:   net.IPv4(192, 168, 0, 0),
					Mask: net.CIDRMask(24, 8*net.IPv4len),
				}},
			},
			args{
				ip: net.IPv4(192, 168, 0, 1),
			},
			true,
		},
		{
			"search ip not presented in list",
			fields{
				subNets: []*net.IPNet{{
					IP:   net.IPv4(192, 168, 0, 0),
					Mask: net.CIDRMask(25, 8*net.IPv4len),
				}},
			},
			args{
				ip: net.IPv4(192, 168, 0, 129),
			},
			false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := &List{subNets: tt.fields.subNets}
			if got := l.Contains(tt.args.ip); got != tt.want {
				t.Errorf("Contains() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestList_Delete(t *testing.T) {
	type fields struct {
		subNets []*net.IPNet
		mu      *sync.RWMutex
	}

	type args struct {
		subnet  *net.IPNet
		wantLen int
	}

	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "delete existing subnet from list",
			fields: fields{
				subNets: []*net.IPNet{{
					IP:   net.IPv4(192, 168, 0, 0),
					Mask: net.CIDRMask(24, 8*net.IPv4len),
				}},
				mu: &sync.RWMutex{},
			},
			args: args{
				subnet: &net.IPNet{
					IP:   net.IPv4(192, 168, 0, 0),
					Mask: net.CIDRMask(24, 8*net.IPv4len),
				},
				wantLen: 0,
			},
		},
		{
			name: "delete not existing subnet from list",
			fields: fields{
				subNets: []*net.IPNet{{
					IP:   net.IPv4(192, 168, 0, 0),
					Mask: net.CIDRMask(24, 8*net.IPv4len),
				}},
				mu: &sync.RWMutex{},
			},
			args: args{
				subnet: &net.IPNet{
					IP:   net.IPv4(192, 168, 0, 0),
					Mask: net.CIDRMask(25, 8*net.IPv4len),
				},
				wantLen: 1,
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := &List{subNets: tt.fields.subNets}
			l.Delete(tt.args.subnet)
			if len(l.subNets) != tt.args.wantLen {
				t.Errorf("want %d elements after removing subnet got %d", tt.args.wantLen, len(l.subNets))
			}
		})
	}
}

// TestRace - just for race detection purposes
func TestRace(t *testing.T) {
	l := NewList()
	wg := sync.WaitGroup{}

	// add subnets
	for i := range make([]struct{}, 128) {
		wg.Add(1)

		go func(i int) {
			defer wg.Done()
			l.Add(&net.IPNet{
				IP:   net.IPv4(10, 0, 1, byte(i%255)),
				Mask: net.CIDRMask(24, 8*net.IPv4len),
			})
		}(i)
	}

	// check subnets
	for i := range make([]struct{}, 1000) {
		wg.Add(1)

		go func(i int) {
			defer wg.Done()
			l.Contains(net.IPv4(10, 0, 1, byte(i%255)))
		}(i)
	}

	// remove subnets
	for i := range make([]struct{}, 50) {
		wg.Add(1)

		go func(i int) {
			defer wg.Done()
			ip := net.IPv4(10, 0, 1, byte(i%255))
			l.Delete(&net.IPNet{
				IP:   ip,
				Mask: net.CIDRMask(24, 8*net.IPv4len),
			})
		}(i)
	}

	wg.Wait()

	want := 128 - 50
	if len(l.subNets) != want {
		t.Errorf("want %d got %d", want, len(l.subNets))
	}
}
