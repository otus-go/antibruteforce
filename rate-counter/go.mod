module rate-counter

go 1.13

require (
	github.com/golang/protobuf v1.3.2
	github.com/kelseyhightower/envconfig v1.4.0
	google.golang.org/grpc v1.25.1
)
