Feature: test whitelists and blacklists

  Scenario: Check if service is listening
    Given api running on host "limiter" and port 8000

  Scenario: Whitelist
    When I add subnet "10.0.0.0" 24 into whitelist
    And I sent 2000 requests with same ip "10.0.0.70" and random login and password
    Then there should be 2000 ok responses and 0 not ok responses

  Scenario: Blacklist
    When I add subnet "10.0.1.0" 24 into blacklist
    And I sent 100 requests with same ip "10.0.1.70" and random login and password
    Then there should be 0 ok responses and 100 not ok responses
