package list

import "net"

type List interface {
	Contains(net.IP) bool
	Add(*net.IPNet)
	Delete(*net.IPNet)
	Clear()
}
