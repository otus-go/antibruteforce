package tests

import (
	"math/rand"
	"strings"
)

var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
	"abcdefghijklmnopqrstuvwxyz" +
	"0123456789"

func getRandomString(length int) string {
	var res strings.Builder
	for i := 0; i < length; i++ {
		res.WriteByte(chars[rand.Intn(len(chars))])
	}
	return res.String()
}
