module integration-tests

go 1.13

require (
	github.com/DATA-DOG/godog v0.7.13
	github.com/golang/protobuf v1.3.2
	google.golang.org/grpc v1.25.1
)
