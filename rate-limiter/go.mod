module rate-limiter

go 1.13

require (
	github.com/golang/protobuf v1.3.2
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/spf13/cobra v0.0.5
	google.golang.org/grpc v1.25.1
)
