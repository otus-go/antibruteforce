package cli

import (
	"fmt"

	"github.com/spf13/cobra"

	"rate-limiter/internal/grpc/limiter"
)

var bucketsCmd = &cobra.Command{
	Use:          "reset-bucket ip login",
	Short:        "reset particular bucket",
	Long:         `reset particular bucket `,
	Args:         cobra.ExactArgs(2),
	SilenceUsage: true,
}

// reset bucket command
func (c *Cli) runBucketE(cmd *cobra.Command, args []string) error {
	_, err := c.client.ResetBuckets(c.ctx, &limiter.ResetBucketsRequest{
		Ip:    args[0],
		Login: args[1],
	})
	if err != nil {
		return err
	}

	fmt.Println("Ok")

	return nil
}
