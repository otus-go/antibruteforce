package counter

import (
	"context"
	"sync"
	"time"
)

// counter - non trade-safe counter implementation,
// should be protected with outer mutex
type counter struct {
	counter   int64
	updatedAt time.Time
}

// return actual value, including leak
func (c *counter) Value(rate float64) int64 {
	value := c.counter - c.CalcLeak(rate)
	if value < 0 {
		return 0
	}

	return value
}

// update timestamp
func (c *counter) Touch() {
	c.updatedAt = time.Now()
}

// calculate leaking value
func (c *counter) CalcLeak(rate float64) int64 {
	timeSinceLastUpdate := float64(time.Since(c.updatedAt))
	return int64(rate * timeSinceLastUpdate)
}

// set counter value
func (c *counter) Set(value int64) {
	if value > 0 {
		c.counter = value
		return
	}

	c.counter = 0
}

// Service - counter service implementation
// represents leaky-bucket algorithm with simple
// "garbage-collector"
type Service struct {
	Limit      int64
	CounterTTL time.Duration

	counters    map[string]*counter
	mu          sync.Mutex
	leakingRate float64
	gcTicker    *time.Ticker
}

func (s *Service) Reset(key string) {
	s.mu.Lock()
	delete(s.counters, key)
	s.mu.Unlock()
}

// Count - returns actual counter value
func (s *Service) Count(key string) (int64, bool) {
	s.mu.Lock()
	defer s.mu.Unlock()

	// create pointer if not exists
	if s.counters[key] == nil {
		s.counters[key] = &counter{}
	}

	ok := false

	//
	value := s.counters[key].Value(s.leakingRate)
	if value < s.Limit {
		value++
		s.counters[key].Set(value)

		ok = true
	}

	// updating timestamp
	s.counters[key].Touch()

	return value, ok
}

// gc - naive garbage collector - runs
// through all counters and removes old ones
func (s *Service) gc() {
	s.mu.Lock()
	for k := range s.counters {
		if time.Since(s.counters[k].updatedAt) >= s.CounterTTL {
			delete(s.counters, k)
		}
	}
	s.mu.Unlock()
}

// runGC - GC scheduler
func (s *Service) runGC(ctx context.Context) {
	for {
		select {
		case <-s.gcTicker.C:
			s.gc()
		case <-ctx.Done():
			s.gcTicker.Stop()
			return
		}
	}
}

// New - create new counter instance, and run GC goroutine
func New(ctx context.Context, limit int64, ttl time.Duration) *Service {
	s := &Service{
		Limit:       limit,
		CounterTTL:  ttl,
		counters:    make(map[string]*counter),
		leakingRate: calcLeakingRate(limit),
		gcTicker:    time.NewTicker(1 * time.Minute),
	}
	go s.runGC(ctx)

	return s
}

// simple helper
func calcLeakingRate(limit int64) float64 {
	return float64(limit) / float64(time.Minute.Nanoseconds())
}
