package limiter

import (
	"context"
	"fmt"
	"log"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"rate-limiter/internal/grpc/counter"
	"rate-limiter/internal/lists"
	"rate-limiter/pkg/interfaces/list"
)

type Server struct {
	sidecar   counter.RateCounterServiceClient
	whitelist list.List
	blacklist list.List
}

func (s *Server) CheckRequest(ctx context.Context, req *CheckRequestRequest) (*CheckRequestResponse, error) {
	ip := net.ParseIP(req.Ip)
	if s.blacklist.Contains(ip) {
		return &CheckRequestResponse{Ok: false}, nil
	}

	if s.whitelist.Contains(ip) {
		return &CheckRequestResponse{Ok: true}, nil
	}

	resp, err := s.sidecar.CountRequests(ctx, &counter.CountRequestsRequest{
		Login:    req.Login,
		Password: req.Password,
		Ip:       req.Ip,
	})
	if err != nil {
		return nil, err
	}

	return &CheckRequestResponse{
		Ok: resp.Ip.Ok && resp.Password.Ok && resp.Login.Ok,
	}, nil
}

func (s *Server) ResetBuckets(
	ctx context.Context, req *ResetBucketsRequest,
) (*ResetBucketsResponse, error) {
	_, err := s.sidecar.ResetCounters(ctx, &counter.ResetCountersRequest{Ip: req.Ip, Login: req.Login})
	if err != nil {
		return nil, err
	}

	return &ResetBucketsResponse{Ok: true}, nil
}

func (s *Server) AddSubNetInBlackList(
	ctx context.Context, req *UpdateSubNetsListRequest,
) (*UpdateSubNetsListResponse, error) {
	subNet := getSubnet(req.Ip, req.Mask)
	s.blacklist.Add(subNet)

	return &UpdateSubNetsListResponse{Ok: true}, nil
}

func (s *Server) RemoveSubNetFromBlackList(
	ctx context.Context, req *UpdateSubNetsListRequest,
) (*UpdateSubNetsListResponse, error) {
	subNet := getSubnet(req.Ip, req.Mask)
	s.blacklist.Delete(subNet)

	return &UpdateSubNetsListResponse{Ok: true}, nil
}

func (s *Server) AddSubNetInWhiteList(
	ctx context.Context, req *UpdateSubNetsListRequest,
) (*UpdateSubNetsListResponse, error) {
	subNet := getSubnet(req.Ip, req.Mask)
	s.whitelist.Add(subNet)

	return &UpdateSubNetsListResponse{Ok: true}, nil
}

func (s *Server) RemoveSubNetFromWhiteList(
	ctx context.Context, req *UpdateSubNetsListRequest,
) (*UpdateSubNetsListResponse, error) {
	subNet := getSubnet(req.Ip, req.Mask)
	s.whitelist.Delete(subNet)

	return &UpdateSubNetsListResponse{Ok: true}, nil
}

func New(
	sidecarClient counter.RateCounterServiceClient,
	whitelist,
	blacklist list.List,
) *Server {
	return &Server{
		sidecar:   sidecarClient,
		whitelist: whitelist,
		blacklist: blacklist,
	}
}

type Config struct {
	RateCounterAddress string `default:"localhost:8001" split_words:"true"`
	Port               int    `default:"8000"`
}

func StartRateLimiterService(ctx context.Context, config *Config) error {
	grpcConn, err := grpc.Dial(
		config.RateCounterAddress,
		grpc.WithInsecure(),
	)
	if err != nil {
		panic(err)
	}

	client := counter.NewRateCounterServiceClient(grpcConn)
	service := New(client, &lists.List{}, &lists.List{})
	address := fmt.Sprintf(":%d", config.Port)

	lis, err := net.Listen("tcp", address)
	if err != nil {
		panic(err)
	}

	grpcServer := grpc.NewServer()

	reflection.Register(grpcServer)
	RegisterAntiBruteForceServiceServer(grpcServer, service)
	log.Printf("Service listening on %s", lis.Addr())

	// looks over complicated,
	// but it let us to run service from test suites
	// and have test coverage for grpc methods
	go func() {
		_ = grpcServer.Serve(lis)
	}()

	go func() {
		<-ctx.Done()
		_ = lis.Close() // nothing to do here :shrug:
		grpcServer.Stop()
	}()

	return nil
}
