// Code generated by protoc-gen-go. DO NOT EDIT.
// source: grpc/limiter/limiter.proto

package limiter

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type CheckRequestRequest struct {
	Login                string   `protobuf:"bytes,1,opt,name=login,proto3" json:"login,omitempty"`
	Password             string   `protobuf:"bytes,2,opt,name=password,proto3" json:"password,omitempty"`
	Ip                   string   `protobuf:"bytes,3,opt,name=ip,proto3" json:"ip,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CheckRequestRequest) Reset()         { *m = CheckRequestRequest{} }
func (m *CheckRequestRequest) String() string { return proto.CompactTextString(m) }
func (*CheckRequestRequest) ProtoMessage()    {}
func (*CheckRequestRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_5efdcf56b8f43f28, []int{0}
}

func (m *CheckRequestRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CheckRequestRequest.Unmarshal(m, b)
}
func (m *CheckRequestRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CheckRequestRequest.Marshal(b, m, deterministic)
}
func (m *CheckRequestRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CheckRequestRequest.Merge(m, src)
}
func (m *CheckRequestRequest) XXX_Size() int {
	return xxx_messageInfo_CheckRequestRequest.Size(m)
}
func (m *CheckRequestRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_CheckRequestRequest.DiscardUnknown(m)
}

var xxx_messageInfo_CheckRequestRequest proto.InternalMessageInfo

func (m *CheckRequestRequest) GetLogin() string {
	if m != nil {
		return m.Login
	}
	return ""
}

func (m *CheckRequestRequest) GetPassword() string {
	if m != nil {
		return m.Password
	}
	return ""
}

func (m *CheckRequestRequest) GetIp() string {
	if m != nil {
		return m.Ip
	}
	return ""
}

type CheckRequestResponse struct {
	Ok                   bool     `protobuf:"varint,1,opt,name=ok,proto3" json:"ok,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CheckRequestResponse) Reset()         { *m = CheckRequestResponse{} }
func (m *CheckRequestResponse) String() string { return proto.CompactTextString(m) }
func (*CheckRequestResponse) ProtoMessage()    {}
func (*CheckRequestResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_5efdcf56b8f43f28, []int{1}
}

func (m *CheckRequestResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CheckRequestResponse.Unmarshal(m, b)
}
func (m *CheckRequestResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CheckRequestResponse.Marshal(b, m, deterministic)
}
func (m *CheckRequestResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CheckRequestResponse.Merge(m, src)
}
func (m *CheckRequestResponse) XXX_Size() int {
	return xxx_messageInfo_CheckRequestResponse.Size(m)
}
func (m *CheckRequestResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_CheckRequestResponse.DiscardUnknown(m)
}

var xxx_messageInfo_CheckRequestResponse proto.InternalMessageInfo

func (m *CheckRequestResponse) GetOk() bool {
	if m != nil {
		return m.Ok
	}
	return false
}

type ResetBucketsRequest struct {
	Login                string   `protobuf:"bytes,1,opt,name=login,proto3" json:"login,omitempty"`
	Ip                   string   `protobuf:"bytes,2,opt,name=ip,proto3" json:"ip,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ResetBucketsRequest) Reset()         { *m = ResetBucketsRequest{} }
func (m *ResetBucketsRequest) String() string { return proto.CompactTextString(m) }
func (*ResetBucketsRequest) ProtoMessage()    {}
func (*ResetBucketsRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_5efdcf56b8f43f28, []int{2}
}

func (m *ResetBucketsRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ResetBucketsRequest.Unmarshal(m, b)
}
func (m *ResetBucketsRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ResetBucketsRequest.Marshal(b, m, deterministic)
}
func (m *ResetBucketsRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ResetBucketsRequest.Merge(m, src)
}
func (m *ResetBucketsRequest) XXX_Size() int {
	return xxx_messageInfo_ResetBucketsRequest.Size(m)
}
func (m *ResetBucketsRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_ResetBucketsRequest.DiscardUnknown(m)
}

var xxx_messageInfo_ResetBucketsRequest proto.InternalMessageInfo

func (m *ResetBucketsRequest) GetLogin() string {
	if m != nil {
		return m.Login
	}
	return ""
}

func (m *ResetBucketsRequest) GetIp() string {
	if m != nil {
		return m.Ip
	}
	return ""
}

type ResetBucketsResponse struct {
	Ok                   bool     `protobuf:"varint,1,opt,name=ok,proto3" json:"ok,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ResetBucketsResponse) Reset()         { *m = ResetBucketsResponse{} }
func (m *ResetBucketsResponse) String() string { return proto.CompactTextString(m) }
func (*ResetBucketsResponse) ProtoMessage()    {}
func (*ResetBucketsResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_5efdcf56b8f43f28, []int{3}
}

func (m *ResetBucketsResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ResetBucketsResponse.Unmarshal(m, b)
}
func (m *ResetBucketsResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ResetBucketsResponse.Marshal(b, m, deterministic)
}
func (m *ResetBucketsResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ResetBucketsResponse.Merge(m, src)
}
func (m *ResetBucketsResponse) XXX_Size() int {
	return xxx_messageInfo_ResetBucketsResponse.Size(m)
}
func (m *ResetBucketsResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_ResetBucketsResponse.DiscardUnknown(m)
}

var xxx_messageInfo_ResetBucketsResponse proto.InternalMessageInfo

func (m *ResetBucketsResponse) GetOk() bool {
	if m != nil {
		return m.Ok
	}
	return false
}

type UpdateSubNetsListRequest struct {
	Ip                   string   `protobuf:"bytes,1,opt,name=ip,proto3" json:"ip,omitempty"`
	Mask                 uint32   `protobuf:"varint,2,opt,name=mask,proto3" json:"mask,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UpdateSubNetsListRequest) Reset()         { *m = UpdateSubNetsListRequest{} }
func (m *UpdateSubNetsListRequest) String() string { return proto.CompactTextString(m) }
func (*UpdateSubNetsListRequest) ProtoMessage()    {}
func (*UpdateSubNetsListRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_5efdcf56b8f43f28, []int{4}
}

func (m *UpdateSubNetsListRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UpdateSubNetsListRequest.Unmarshal(m, b)
}
func (m *UpdateSubNetsListRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UpdateSubNetsListRequest.Marshal(b, m, deterministic)
}
func (m *UpdateSubNetsListRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UpdateSubNetsListRequest.Merge(m, src)
}
func (m *UpdateSubNetsListRequest) XXX_Size() int {
	return xxx_messageInfo_UpdateSubNetsListRequest.Size(m)
}
func (m *UpdateSubNetsListRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_UpdateSubNetsListRequest.DiscardUnknown(m)
}

var xxx_messageInfo_UpdateSubNetsListRequest proto.InternalMessageInfo

func (m *UpdateSubNetsListRequest) GetIp() string {
	if m != nil {
		return m.Ip
	}
	return ""
}

func (m *UpdateSubNetsListRequest) GetMask() uint32 {
	if m != nil {
		return m.Mask
	}
	return 0
}

type UpdateSubNetsListResponse struct {
	Ok                   bool     `protobuf:"varint,1,opt,name=ok,proto3" json:"ok,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UpdateSubNetsListResponse) Reset()         { *m = UpdateSubNetsListResponse{} }
func (m *UpdateSubNetsListResponse) String() string { return proto.CompactTextString(m) }
func (*UpdateSubNetsListResponse) ProtoMessage()    {}
func (*UpdateSubNetsListResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_5efdcf56b8f43f28, []int{5}
}

func (m *UpdateSubNetsListResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UpdateSubNetsListResponse.Unmarshal(m, b)
}
func (m *UpdateSubNetsListResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UpdateSubNetsListResponse.Marshal(b, m, deterministic)
}
func (m *UpdateSubNetsListResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UpdateSubNetsListResponse.Merge(m, src)
}
func (m *UpdateSubNetsListResponse) XXX_Size() int {
	return xxx_messageInfo_UpdateSubNetsListResponse.Size(m)
}
func (m *UpdateSubNetsListResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_UpdateSubNetsListResponse.DiscardUnknown(m)
}

var xxx_messageInfo_UpdateSubNetsListResponse proto.InternalMessageInfo

func (m *UpdateSubNetsListResponse) GetOk() bool {
	if m != nil {
		return m.Ok
	}
	return false
}

func init() {
	proto.RegisterType((*CheckRequestRequest)(nil), "CheckRequestRequest")
	proto.RegisterType((*CheckRequestResponse)(nil), "CheckRequestResponse")
	proto.RegisterType((*ResetBucketsRequest)(nil), "ResetBucketsRequest")
	proto.RegisterType((*ResetBucketsResponse)(nil), "ResetBucketsResponse")
	proto.RegisterType((*UpdateSubNetsListRequest)(nil), "UpdateSubNetsListRequest")
	proto.RegisterType((*UpdateSubNetsListResponse)(nil), "UpdateSubNetsListResponse")
}

func init() { proto.RegisterFile("grpc/limiter/limiter.proto", fileDescriptor_5efdcf56b8f43f28) }

var fileDescriptor_5efdcf56b8f43f28 = []byte{
	// 325 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xac, 0x53, 0x4f, 0x4b, 0xc3, 0x30,
	0x14, 0xdf, 0x3a, 0x95, 0xf9, 0x50, 0x0f, 0x59, 0x07, 0x5d, 0x4f, 0x92, 0x83, 0x08, 0x42, 0x04,
	0x3d, 0x8a, 0xc2, 0x26, 0x0c, 0x04, 0x51, 0xe8, 0x94, 0x9d, 0xbb, 0xf6, 0xb1, 0x85, 0xac, 0x4d,
	0x4c, 0xd2, 0xf9, 0x81, 0xfd, 0x22, 0xb2, 0x74, 0xd3, 0x0d, 0x32, 0x2f, 0xee, 0xf4, 0xf2, 0xf2,
	0xf2, 0x7e, 0xff, 0x20, 0x10, 0x4f, 0xb5, 0xca, 0xae, 0xe7, 0xbc, 0xe0, 0x16, 0xf5, 0xba, 0x32,
	0xa5, 0xa5, 0x95, 0x74, 0x0c, 0x9d, 0xc7, 0x19, 0x66, 0x22, 0xc1, 0x8f, 0x0a, 0x8d, 0x5d, 0x15,
	0x12, 0xc2, 0xe1, 0x5c, 0x4e, 0x79, 0x19, 0x35, 0xcf, 0x9b, 0x97, 0xc7, 0x49, 0xdd, 0x90, 0x18,
	0xda, 0x2a, 0x35, 0xe6, 0x53, 0xea, 0x3c, 0x0a, 0xdc, 0xe0, 0xa7, 0x27, 0x67, 0x10, 0x70, 0x15,
	0xb5, 0xdc, 0x6d, 0xc0, 0x15, 0xbd, 0x80, 0x70, 0x1b, 0xd8, 0x28, 0x59, 0x1a, 0x5c, 0xbe, 0x93,
	0xc2, 0xc1, 0xb6, 0x93, 0x40, 0x0a, 0x7a, 0x07, 0x9d, 0x04, 0x0d, 0xda, 0x41, 0x95, 0x09, 0xb4,
	0xe6, 0x6f, 0x01, 0x35, 0x49, 0xb0, 0x49, 0xb2, 0xbd, 0xbc, 0x83, 0xe4, 0x01, 0xa2, 0x77, 0x95,
	0xa7, 0x16, 0x47, 0xd5, 0xe4, 0x05, 0xad, 0x79, 0xe6, 0xbf, 0x56, 0x6b, 0xcc, 0xe6, 0x1a, 0x93,
	0x10, 0x38, 0x28, 0x52, 0x23, 0x1c, 0xcb, 0x69, 0xe2, 0xce, 0xf4, 0x0a, 0x7a, 0x9e, 0x7d, 0x3f,
	0xd9, 0xcd, 0x57, 0x0b, 0xba, 0xfd, 0xd2, 0xf2, 0x81, 0xae, 0x2c, 0x0e, 0xa5, 0xce, 0x70, 0x84,
	0x7a, 0xc1, 0x33, 0x24, 0xf7, 0x70, 0xb2, 0x99, 0x09, 0x09, 0x99, 0x27, 0xfb, 0xb8, 0xcb, 0x7c,
	0xc1, 0xd1, 0xc6, 0x72, 0x7d, 0xd3, 0x2d, 0x09, 0x99, 0x27, 0xb9, 0xb8, 0xcb, 0x7c, 0x91, 0xd0,
	0x06, 0x79, 0x85, 0xb0, 0x9f, 0xe7, 0xb5, 0x83, 0xa7, 0x72, 0x30, 0x4f, 0x33, 0xb1, 0xf4, 0x41,
	0x7a, 0x6c, 0x57, 0x36, 0x71, 0xcc, 0x76, 0xda, 0xa6, 0x0d, 0xf2, 0x06, 0xbd, 0x04, 0x0b, 0xb9,
	0x58, 0x8d, 0x87, 0x5a, 0x16, 0x7b, 0x40, 0xdd, 0x96, 0x39, 0x9e, 0x71, 0x8b, 0x7b, 0x97, 0xf9,
	0x7f, 0xd4, 0xc9, 0x91, 0xfb, 0x3f, 0xb7, 0xdf, 0x01, 0x00, 0x00, 0xff, 0xff, 0x74, 0x82, 0x95,
	0xd6, 0x5d, 0x03, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// AntiBruteForceServiceClient is the client API for AntiBruteForceService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type AntiBruteForceServiceClient interface {
	CheckRequest(ctx context.Context, in *CheckRequestRequest, opts ...grpc.CallOption) (*CheckRequestResponse, error)
	ResetBuckets(ctx context.Context, in *ResetBucketsRequest, opts ...grpc.CallOption) (*ResetBucketsResponse, error)
	AddSubNetInBlackList(ctx context.Context, in *UpdateSubNetsListRequest, opts ...grpc.CallOption) (*UpdateSubNetsListResponse, error)
	RemoveSubNetFromBlackList(ctx context.Context, in *UpdateSubNetsListRequest, opts ...grpc.CallOption) (*UpdateSubNetsListResponse, error)
	AddSubNetInWhiteList(ctx context.Context, in *UpdateSubNetsListRequest, opts ...grpc.CallOption) (*UpdateSubNetsListResponse, error)
	RemoveSubNetFromWhiteList(ctx context.Context, in *UpdateSubNetsListRequest, opts ...grpc.CallOption) (*UpdateSubNetsListResponse, error)
}

type antiBruteForceServiceClient struct {
	cc *grpc.ClientConn
}

func NewAntiBruteForceServiceClient(cc *grpc.ClientConn) AntiBruteForceServiceClient {
	return &antiBruteForceServiceClient{cc}
}

func (c *antiBruteForceServiceClient) CheckRequest(ctx context.Context, in *CheckRequestRequest, opts ...grpc.CallOption) (*CheckRequestResponse, error) {
	out := new(CheckRequestResponse)
	err := c.cc.Invoke(ctx, "/AntiBruteForceService/CheckRequest", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *antiBruteForceServiceClient) ResetBuckets(ctx context.Context, in *ResetBucketsRequest, opts ...grpc.CallOption) (*ResetBucketsResponse, error) {
	out := new(ResetBucketsResponse)
	err := c.cc.Invoke(ctx, "/AntiBruteForceService/ResetBuckets", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *antiBruteForceServiceClient) AddSubNetInBlackList(ctx context.Context, in *UpdateSubNetsListRequest, opts ...grpc.CallOption) (*UpdateSubNetsListResponse, error) {
	out := new(UpdateSubNetsListResponse)
	err := c.cc.Invoke(ctx, "/AntiBruteForceService/AddSubNetInBlackList", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *antiBruteForceServiceClient) RemoveSubNetFromBlackList(ctx context.Context, in *UpdateSubNetsListRequest, opts ...grpc.CallOption) (*UpdateSubNetsListResponse, error) {
	out := new(UpdateSubNetsListResponse)
	err := c.cc.Invoke(ctx, "/AntiBruteForceService/RemoveSubNetFromBlackList", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *antiBruteForceServiceClient) AddSubNetInWhiteList(ctx context.Context, in *UpdateSubNetsListRequest, opts ...grpc.CallOption) (*UpdateSubNetsListResponse, error) {
	out := new(UpdateSubNetsListResponse)
	err := c.cc.Invoke(ctx, "/AntiBruteForceService/AddSubNetInWhiteList", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *antiBruteForceServiceClient) RemoveSubNetFromWhiteList(ctx context.Context, in *UpdateSubNetsListRequest, opts ...grpc.CallOption) (*UpdateSubNetsListResponse, error) {
	out := new(UpdateSubNetsListResponse)
	err := c.cc.Invoke(ctx, "/AntiBruteForceService/RemoveSubNetFromWhiteList", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// AntiBruteForceServiceServer is the server API for AntiBruteForceService service.
type AntiBruteForceServiceServer interface {
	CheckRequest(context.Context, *CheckRequestRequest) (*CheckRequestResponse, error)
	ResetBuckets(context.Context, *ResetBucketsRequest) (*ResetBucketsResponse, error)
	AddSubNetInBlackList(context.Context, *UpdateSubNetsListRequest) (*UpdateSubNetsListResponse, error)
	RemoveSubNetFromBlackList(context.Context, *UpdateSubNetsListRequest) (*UpdateSubNetsListResponse, error)
	AddSubNetInWhiteList(context.Context, *UpdateSubNetsListRequest) (*UpdateSubNetsListResponse, error)
	RemoveSubNetFromWhiteList(context.Context, *UpdateSubNetsListRequest) (*UpdateSubNetsListResponse, error)
}

// UnimplementedAntiBruteForceServiceServer can be embedded to have forward compatible implementations.
type UnimplementedAntiBruteForceServiceServer struct {
}

func (*UnimplementedAntiBruteForceServiceServer) CheckRequest(ctx context.Context, req *CheckRequestRequest) (*CheckRequestResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CheckRequest not implemented")
}
func (*UnimplementedAntiBruteForceServiceServer) ResetBuckets(ctx context.Context, req *ResetBucketsRequest) (*ResetBucketsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ResetBuckets not implemented")
}
func (*UnimplementedAntiBruteForceServiceServer) AddSubNetInBlackList(ctx context.Context, req *UpdateSubNetsListRequest) (*UpdateSubNetsListResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AddSubNetInBlackList not implemented")
}
func (*UnimplementedAntiBruteForceServiceServer) RemoveSubNetFromBlackList(ctx context.Context, req *UpdateSubNetsListRequest) (*UpdateSubNetsListResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method RemoveSubNetFromBlackList not implemented")
}
func (*UnimplementedAntiBruteForceServiceServer) AddSubNetInWhiteList(ctx context.Context, req *UpdateSubNetsListRequest) (*UpdateSubNetsListResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AddSubNetInWhiteList not implemented")
}
func (*UnimplementedAntiBruteForceServiceServer) RemoveSubNetFromWhiteList(ctx context.Context, req *UpdateSubNetsListRequest) (*UpdateSubNetsListResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method RemoveSubNetFromWhiteList not implemented")
}

func RegisterAntiBruteForceServiceServer(s *grpc.Server, srv AntiBruteForceServiceServer) {
	s.RegisterService(&_AntiBruteForceService_serviceDesc, srv)
}

func _AntiBruteForceService_CheckRequest_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CheckRequestRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AntiBruteForceServiceServer).CheckRequest(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/AntiBruteForceService/CheckRequest",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AntiBruteForceServiceServer).CheckRequest(ctx, req.(*CheckRequestRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _AntiBruteForceService_ResetBuckets_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ResetBucketsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AntiBruteForceServiceServer).ResetBuckets(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/AntiBruteForceService/ResetBuckets",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AntiBruteForceServiceServer).ResetBuckets(ctx, req.(*ResetBucketsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _AntiBruteForceService_AddSubNetInBlackList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateSubNetsListRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AntiBruteForceServiceServer).AddSubNetInBlackList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/AntiBruteForceService/AddSubNetInBlackList",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AntiBruteForceServiceServer).AddSubNetInBlackList(ctx, req.(*UpdateSubNetsListRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _AntiBruteForceService_RemoveSubNetFromBlackList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateSubNetsListRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AntiBruteForceServiceServer).RemoveSubNetFromBlackList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/AntiBruteForceService/RemoveSubNetFromBlackList",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AntiBruteForceServiceServer).RemoveSubNetFromBlackList(ctx, req.(*UpdateSubNetsListRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _AntiBruteForceService_AddSubNetInWhiteList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateSubNetsListRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AntiBruteForceServiceServer).AddSubNetInWhiteList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/AntiBruteForceService/AddSubNetInWhiteList",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AntiBruteForceServiceServer).AddSubNetInWhiteList(ctx, req.(*UpdateSubNetsListRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _AntiBruteForceService_RemoveSubNetFromWhiteList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateSubNetsListRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AntiBruteForceServiceServer).RemoveSubNetFromWhiteList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/AntiBruteForceService/RemoveSubNetFromWhiteList",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AntiBruteForceServiceServer).RemoveSubNetFromWhiteList(ctx, req.(*UpdateSubNetsListRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _AntiBruteForceService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "AntiBruteForceService",
	HandlerType: (*AntiBruteForceServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CheckRequest",
			Handler:    _AntiBruteForceService_CheckRequest_Handler,
		},
		{
			MethodName: "ResetBuckets",
			Handler:    _AntiBruteForceService_ResetBuckets_Handler,
		},
		{
			MethodName: "AddSubNetInBlackList",
			Handler:    _AntiBruteForceService_AddSubNetInBlackList_Handler,
		},
		{
			MethodName: "RemoveSubNetFromBlackList",
			Handler:    _AntiBruteForceService_RemoveSubNetFromBlackList_Handler,
		},
		{
			MethodName: "AddSubNetInWhiteList",
			Handler:    _AntiBruteForceService_AddSubNetInWhiteList_Handler,
		},
		{
			MethodName: "RemoveSubNetFromWhiteList",
			Handler:    _AntiBruteForceService_RemoveSubNetFromWhiteList_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "grpc/limiter/limiter.proto",
}
