package limiter

import (
	"net"
	"testing"

	"google.golang.org/grpc"

	"rate-limiter/internal/grpc/counter"
)

func getRateCounterServer() (net.Listener, *grpc.Server) {
	sidecarService := &counter.UnimplementedRateCounterServiceServer{}

	lis, err := net.Listen("tcp", "127.0.0.1:8001")
	if err != nil {
		panic(err)
	}

	grpcServer := grpc.NewServer()

	counter.RegisterRateCounterServiceServer(grpcServer, sidecarService)

	return lis, grpcServer
}

func getSubnet(ip string, mask uint32) *net.IPNet {
	subNet := &net.IPNet{
		IP:   net.ParseIP(ip),
		Mask: net.CIDRMask(int(mask), 8*net.IPv4len),
	}

	return subNet
}

func fatalIfErr(t *testing.T, err error) {
	t.Helper()

	if err != nil {
		t.Fatal(err)
	}
}
