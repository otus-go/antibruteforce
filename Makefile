.PHONY: build

run:
	docker-compose -f deployments/docker-compose.yaml up -d --build

stop:
	docker-compose -f deployments/docker-compose.yaml down

restart: stop run

test:
	set -e ;\
	docker-compose -f deployments/docker-compose.test.yaml build ;\
	docker-compose -f deployments/docker-compose.test.yaml run integration-tests \
	  go test -mod=vendor -v;\
	test_status_code=$$? ;\
	docker-compose -f deployments/docker-compose.test.yaml down ;\
	exit $$test_status_code

stop-test:
	docker-compose -f deployments/docker-compose.test.yaml down

protos:
	protoc -I/usr/local/include -I. \
			-I$$GOPATH/src \
			-I$$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
			--go_out=plugins=grpc:integration-tests \
			./grpc/limiter/limiter.proto

	protoc -I/usr/local/include -I. \
    		-I$$GOPATH/src \
    		-I$$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
    		--go_out=plugins=grpc:rate-limiter/internal \
    		./grpc/limiter/limiter.proto

	protoc -I/usr/local/include -I. \
			-I$$GOPATH/src \
			-I$$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
			--go_out=plugins=grpc:rate-limiter/internal \
			./grpc/counter/counter.proto

	protoc -I/usr/local/include -I. \
			-I$$GOPATH/src \
			-I$$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
			--go_out=plugins=grpc:rate-counter/internal \
			./grpc/counter/counter.proto
