# Antibruteforce

Итоговый проект курса Разработчик Go в OTUS

[ТЗ для реализации сервиса](https://github.com/OtusTeam/Go/blob/master/project-antibruteforce.md)

## Quick start

```
$ make run        # docker-compose up --build -d
$ make stop       # docker-compose down
$ make restart    # stop run

$ make test       # run godog tests
$ make stop-test  # stop compose if tests failed
```

Веб-интерфейс через который можно подергать сервис
поднимается на порте `8080`

Основной сервис запускается на порте `8000`, сайдкар на `8001`,
порты проброшены и доступны с хоста.

При запущенном `docker-compose` можно подключиться к контейнеру с api 
и поиграться там с `cli`, позволяющим добавить/удалить подсети в черный
или белый список, или сбросить бакеты.

```
$ docker-compose exec limiter sh
$ limiter-config

limiter-config is a cli for managing antibruteforce util

Usage:
  limiter-config [command]

Available Commands:
  blacklists   blacklists is a subcommand for managing blacklists
  help         Help about any command
  reset-bucket reset particular bucket
  whitelists   whitelists is a subcommand for managing whitelists

Flags:
  -h, --help          help for limiter-config
      --host string   set limiter hostname (default "localhost")
  -p, --port int      set limiter service port (default 8000)

Use "limiter-config [command] --help" for more information about a command.
```

Либо можно вызвать утилиту через `docker-compose run`,
но тогда нужно явно указать host.

Например:

```
$ docker-compose run limiter \
  limiter-config blacklists add --host=limiter 10.0.0.0 24
```

## Описание

Монорепозиторий содержит три модуля: интеграционные тесты и два сервиса
- мне показалось удобным вынести счетчики в отдельный сайдкар-сервис,
чтобы его можно было переписывать как душе угодно,
используя внешние хранилища и т.д.,
и не трогать при этом сервис, предоставляющий "публичный" api.

Сервисы общаются по gRPC, `cli` внутри тоже использует gRPC клиент.

## Тесты
То что не зависит от gRPC покрыто юнит-тестами, то что зависит - интерграционными.
Часть интеграционных тестов написана с помощью обычного модуля `testing`,
и запускается вместе с юнит-тестами в CI при каждом коммите. 

Функциональные тесты написаны с использованием `godog` и вынесены в отдельный модуль.

## Настройки

Настройки сервисов осуществляются через переменные окружения.

### API

|  Variable                      | Default      | Description                      |
|--------------------------------|--------------|----------------------------------|
| LIMITER_RATE_COUNTER_ADDRESS   | counter:8001 | адрес сервиса-счетчика           |
| LIMITER_PORT                   | 8000         | порт на котором запускается API  |

### Counter

|  Variable             | Default  | Description                                  |
|-----------------------|----------|----------------------------------------------|
| COUNTER_LOGIN_LIMIT   | 10       | ограничение на запросы с одинаковым логином  |
| COUNTER_PASSWORD_LIMIT| 100      | ограничение на запросы с одинаковым паролем  |
| COUNTER_IP_LIMIT      | 1000     | ограничение на запросы с одинаковым IP       |
| COUNTER_PORT          | 8001     | порт на котором запускается сервис-счетчик   |
