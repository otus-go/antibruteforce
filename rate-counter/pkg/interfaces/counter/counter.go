package counter

type Counter interface {
	Count(key string) (int64, bool)
}
