package counter

import (
	"context"
	"testing"

	"github.com/kelseyhightower/envconfig"
	"google.golang.org/grpc"
)

var (
	listenAddr = "127.0.0.1:8001"
)

func getGrpcConn(t *testing.T) *grpc.ClientConn {
	grcpConn, err := grpc.Dial(listenAddr, grpc.WithInsecure())
	if err != nil {
		t.Fatalf("cant connect to grpc: %v", err)
	}

	return grcpConn
}

// The Tsar test case.
// Runs service and test both grpc methods
func TestServer_Integration(t *testing.T) {
	config := &Config{}
	_ = envconfig.Process("counter", config)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel() // deferring cancel for case if function will not finished correctly

	err := StartCounterService(ctx, config)
	fatalIfErr(t, err)

	conn := getGrpcConn(t)
	defer conn.Close()

	client := NewRateCounterServiceClient(conn)

	var countResp *CountRequestsResponse

	// Increase counters
	for i := 0; i < 101; i++ {
		countResp, err = client.CountRequests(ctx, &CountRequestsRequest{
			Login: "login", Password: "password", Ip: "127.0.0.1"})
		fatalIfErr(t, err)
	}

	if countResp.Login.Ok {
		t.Errorf("want not ok for login counter")
	}

	if countResp.Login.Count != countResp.Login.Limit {
		t.Errorf("want %d got %d", countResp.Login.Limit, countResp.Login.Count)
	}

	if countResp.Password.Ok {
		t.Errorf("want not ok for password counter")
	}

	if countResp.Password.Count != countResp.Password.Limit {
		t.Errorf("want %d got %d for password count", countResp.Login.Limit, countResp.Login.Count)
	}

	if !countResp.Ip.Ok {
		t.Errorf("want ok for ip counter")
	}

	// Test Reset
	_, err = client.ResetCounters(ctx, &ResetCountersRequest{Login: "login", Ip: "127.0.0.1"})
	fatalIfErr(t, err)

	countResp, err = client.CountRequests(
		ctx, &CountRequestsRequest{Login: "login", Password: "password", Ip: "127.0.0.1"})
	fatalIfErr(t, err)

	if countResp.Ip.Count != 1 {
		t.Error("ip counter must be reset")
	}

	if countResp.Login.Count != 1 {
		t.Error("login counter must be reset")
	}

	cancel() // calling cancel explicitly
}

func fatalIfErr(t *testing.T, err error) {
	t.Helper()

	if err != nil {
		t.Fatal(err)
	}
}
