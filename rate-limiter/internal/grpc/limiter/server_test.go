package limiter

import (
	"context"
	"testing"

	"github.com/kelseyhightower/envconfig"
	"google.golang.org/grpc"
	//"google.golang.org/grpc/status"
)

var (
	listenAddr = "127.0.0.1:8000"
)

// create insecure grpc connection
func getGrpcConn(t *testing.T) *grpc.ClientConn {
	grcpConn, err := grpc.Dial(
		listenAddr,
		grpc.WithInsecure(),
	)
	if err != nil {
		t.Fatalf("cant connect to grpc: %v", err)
	}

	return grcpConn
}

// The Tsar test case.
// Runs service and tests if whitelist and blacklist works
func TestServer_Integration(t *testing.T) {
	// run sidecar stub
	lis, grpcServer := getRateCounterServer()
	defer grpcServer.Stop()

	go func() {
		_ = grpcServer.Serve(lis)
	}()

	// run service
	config := &Config{}
	_ = envconfig.Process("counter", config)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel() // deferring cancel for case if function will not finished correctly

	err := StartRateLimiterService(ctx, config)
	fatalIfErr(t, err)

	conn := getGrpcConn(t)
	defer conn.Close()

	client := NewAntiBruteForceServiceClient(conn)

	var checkReqResp *CheckRequestResponse

	t.Run("add to blacklist", func(t *testing.T) {
		_, err = client.AddSubNetInBlackList(ctx, &UpdateSubNetsListRequest{Ip: "10.0.2.0", Mask: 25})
		fatalIfErr(t, err)
	})

	t.Run("add to whitelist", func(t *testing.T) {
		_, err = client.AddSubNetInWhiteList(ctx, &UpdateSubNetsListRequest{Ip: "10.0.2.128", Mask: 25})
		fatalIfErr(t, err)
	})

	t.Run("test whitelisted subnet", func(t *testing.T) {
		checkReqResp, err = client.CheckRequest(ctx, &CheckRequestRequest{
			Login: "login", Password: "password", Ip: "10.0.2.135"})
		fatalIfErr(t, err)

		if !checkReqResp.Ok {
			t.Error("result must be ok for whitelisted subnet")
		}
	})

	// from blacklist
	t.Run("test blacklisted subnet", func(t *testing.T) {
		checkReqResp, err = client.CheckRequest(ctx, &CheckRequestRequest{
			Login: "login", Password: "password", Ip: "10.0.2.70"})
		fatalIfErr(t, err)

		if checkReqResp.Ok {
			t.Error("result must be not ok for blacklisted subnet")
		}
	})

	cancel() // calling cancel explicitly
}
