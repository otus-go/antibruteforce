package counter

import (
	"context"
	"fmt"
	"log"
	"net"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"rate-counter/internal/counter"
)

type Config struct {
	LoginLimit    int64 `default:"10" split_words:"true"`
	PasswordLimit int64 `default:"100" split_words:"true"`
	IPLimit       int64 `envconfig:"ip_limit" default:"1000"`

	Port int `default:"8001"`
}

type Server struct {
	ipCounter       *counter.Service
	loginCounter    *counter.Service
	passwordCounter *counter.Service
}

func (s *Server) CountRequests(ctx context.Context, req *CountRequestsRequest) (*CountRequestsResponse, error) {
	ipCount, ipOk := s.ipCounter.Count(req.Ip)
	loginCount, loginOk := s.loginCounter.Count(req.Login)
	passCount, passOk := s.passwordCounter.Count(req.Password)

	return &CountRequestsResponse{
		Login: &Counter{
			Count: loginCount,
			Limit: s.loginCounter.Limit,
			Ok:    loginOk,
		},
		Password: &Counter{
			Count: passCount,
			Limit: s.passwordCounter.Limit,
			Ok:    passOk,
		},
		Ip: &Counter{
			Count: ipCount,
			Limit: s.ipCounter.Limit,
			Ok:    ipOk,
		},
	}, nil
}

func (s *Server) ResetCounters(ctx context.Context, req *ResetCountersRequest) (*ResetCountersResponse, error) {
	s.ipCounter.Reset(req.Ip)
	s.loginCounter.Reset(req.Login)

	return &ResetCountersResponse{Ok: true}, nil
}

func New(ctx context.Context, loginLimit, passwordLimit, ipLimit int64) *Server {
	counterTTL := 5 * time.Minute
	ipCounter := counter.New(ctx, ipLimit, counterTTL)
	loginCounter := counter.New(ctx, loginLimit, counterTTL)
	passwordCounter := counter.New(ctx, passwordLimit, counterTTL)

	return &Server{
		ipCounter:       ipCounter,
		loginCounter:    loginCounter,
		passwordCounter: passwordCounter,
	}
}

func StartCounterService(ctx context.Context, config *Config) error {
	service := New(ctx, config.LoginLimit, config.PasswordLimit, config.IPLimit)
	address := fmt.Sprintf(":%d", config.Port)

	lis, err := net.Listen("tcp", address)
	if err != nil {
		panic(err)
	}

	grpcServer := grpc.NewServer()

	reflection.Register(grpcServer)
	RegisterRateCounterServiceServer(grpcServer, service)
	log.Printf("Service listening on %s", lis.Addr())

	// looks over complicated,
	// but it let us to run service from test suites
	// and have test coverage for grpc methods
	go func() {
		_ = grpcServer.Serve(lis)
	}()

	go func() {
		<-ctx.Done()
		_ = lis.Close() // nothing to do here :shrug:
		grpcServer.Stop()
	}()

	return nil
}
