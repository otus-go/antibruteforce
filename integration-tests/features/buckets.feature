Feature: Test buckets api
  Test if buckets works

  Scenario: Check if service is listening
    Given api running on host "limiter" and port 8000

  Scenario: Limiting by login
    When I send 20 requests with same login "login"
    Then there should be 10 ok responses and 10 not ok responses
    And I send 10 requests with same login "qwerty"
    Then there should be 10 ok responses and 0 not ok responses

  Scenario: Limiting by password
    When I send 200 requests with same password "password" and random login
    Then there should be 100 ok responses and 100 not ok responses

  Scenario: Limiting by ip
    When I sent 2000 requests with same ip "10.0.2.193" and random login and password
    Then there should be 1000 ok responses and 1000 not ok responses

  Scenario: Reset bucket
    When I send 20 requests with same login "login2"
    Then there should be 10 ok responses and 10 not ok responses
    When I reset bucket for login "login2" and IP "10.0.2.110"
    And  I send 10 requests with same login "login2"
    Then there should be 10 ok responses and 0 not ok responses
