package tests

import (
	"os"
	"testing"

	"github.com/DATA-DOG/godog"
)

func TestMain(m *testing.M) {
	status := godog.RunWithOptions("integration", func(s *godog.Suite) {
		FeatureContext(s)
	}, godog.Options{
		Format:        "pretty",
		Paths:         []string{"features"},
		Randomize:     0, // Последовательный порядок исполнения
		StopOnFailure: true,
	})

	if st := m.Run(); st > status {
		status = st
	}

	os.Exit(status)
}
