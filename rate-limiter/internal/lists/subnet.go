package lists

import (
	"net"
	"sync"
)

// List type - common type for whitelist and blacklists
// Naive and quite primitive implementation,
// Search and Delete methods have O(n) time complexity
// Probably in real world application it would be better to use
// some kind of sorted collection for O(ln(n)) search
type List struct {
	subNets []*net.IPNet
	mu      sync.RWMutex
}

// NewList - constructor for List type
func NewList(nets ...*net.IPNet) *List {
	return &List{subNets: nets}
}

// Add - add new subnet into whitelist or blacklist
func (l *List) Add(subnet *net.IPNet) {
	l.mu.Lock()
	l.subNets = append(l.subNets, subnet)
	l.mu.Unlock()
}

// Contains - check if particular IP address matches with one of subnets in list
func (l *List) Contains(ip net.IP) bool {
	l.mu.RLock()
	defer l.mu.RUnlock()

	for i := range l.subNets {
		if l.subNets[i].Contains(ip) {
			return true
		}
	}

	return false
}

// Delete - remove particular subnet from the list
func (l *List) Delete(subnet *net.IPNet) {
	l.mu.Lock()
	for i := 0; i < len(l.subNets); i++ {
		if l.subNets[i].String() == subnet.String() {
			l.subNets[i] = nil // slice contains pointers, prevent memory lick
			l.subNets = append(l.subNets[:i], l.subNets[i+1:]...)
		}
	}
	l.mu.Unlock()
}

// Clear - clear list content
func (l *List) Clear() {
	l.mu.Lock()
	l.subNets = nil
	l.mu.Unlock()
}
