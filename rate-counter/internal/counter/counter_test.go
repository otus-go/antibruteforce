package counter

import (
	"context"
	"testing"
	"time"
)

func TestService_Count(t *testing.T) {
	cases := []struct {
		name  string
		limit int64
		iter  int
	}{
		{
			name:  "count only once",
			limit: 1,
			iter:  1,
		},
		{
			name:  "count hundred times",
			limit: 1,
			iter:  100,
		},
		{
			name:  "count thousand times",
			limit: 100,
			iter:  1000,
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			s := New(ctx, tt.limit, 1*time.Minute)
			cancel()
			var got int64
			for i := 0; i < tt.iter; i++ {
				got, _ = s.Count("key")
			}
			if got != tt.limit {
				t.Errorf("want %d got %d", tt.limit, got)
			}
		})
	}
}

func TestService_Reset(t *testing.T) {
	s := Service{
		Limit: 1000,
		counters: map[string]*counter{
			"counter": {
				counter:   1000,
				updatedAt: time.Now().Add(-5 * time.Minute),
			},
		},
		CounterTTL: 5 * time.Minute,
	}

	s.Reset("counter")

	if s.counters["counter"] != nil {
		t.Errorf("counter should be reseted")
	}
}

func TestService_gc(t *testing.T) {
	outdatedCounter := &counter{
		counter:   1000,
		updatedAt: time.Now().Add(-5 * time.Minute),
	}

	freshCounter := &counter{
		counter:   1000,
		updatedAt: time.Now().Add(-4 * time.Minute),
	}

	s := Service{
		Limit: 1000,
		counters: map[string]*counter{
			"outdated": outdatedCounter,
			"fresh":    freshCounter,
		},
		CounterTTL: 5 * time.Minute,
	}
	s.gc()

	if s.counters["outdated"] != nil {
		t.Errorf("outdated counter not collected with gc")
	}

	if s.counters["fresh"] == nil {
		t.Errorf("fresh counter collected with gc")
	}
}
