package tests

import (
	"context"
	"fmt"
	"math/rand"
	"time"

	"github.com/DATA-DOG/godog"
	"github.com/DATA-DOG/godog/gherkin"
	"google.golang.org/grpc"

	pb "integration-tests/grpc/limiter"
)

type apiTest struct {
	grpcConnect *grpc.ClientConn
	grpcClient  pb.AntiBruteForceServiceClient

	results map[bool]int

	ctx context.Context
}

// set up hook
func seedRandom(*gherkin.Feature) {
	rand.Seed(time.Now().UnixNano())
}

// tear down hook
func (t *apiTest) closeConnection(*gherkin.Feature) {
	if t.grpcClient != nil {
		_ = t.grpcConnect.Close()
	}
}

// create connection and client
func (t *apiTest) apiRunningOnHostAndPort(host string, port int) error {

	address := fmt.Sprintf("%s:%d", host, port)

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	grpcConn, err := grpc.DialContext(ctx, address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		return fmt.Errorf("couldn't connect to service: %w", err)
	}

	t.grpcConnect = grpcConn
	t.grpcClient = pb.NewAntiBruteForceServiceClient(grpcConn)

	return nil
}

func (t *apiTest) iSendRequestsWithSameLogin(requestsCount int, login string) error {
	return t.sendRequests(requestsCount, login, "", "10.0.2.110")
}

func (t *apiTest) iSendRequestsWithSamePasswordAndRandomLogin(requestsCount int, password string) error {
	return t.sendRequests(requestsCount, "", "password", "10.0.2.110")
}

func (t *apiTest) iSentRequestsWithSameIpAndRandomLoginAndPassword(requestsCount int, ip string) error {
	return t.sendRequests(requestsCount, "", "", ip)
}

func (t *apiTest) iAddSubnetIntoWhitelist(ip string, mask int) error {
	_, err := t.grpcClient.AddSubNetInWhiteList(t.ctx, &pb.UpdateSubNetsListRequest{
		Ip:   ip,
		Mask: uint32(mask),
	})
	return err
}

func (t *apiTest) iAddSubnetIntoBlacklist(ip string, mask int) error {
	_, err := t.grpcClient.AddSubNetInBlackList(t.ctx, &pb.UpdateSubNetsListRequest{
		Ip:   ip,
		Mask: uint32(mask),
	})
	return err
}

func (t *apiTest) iResetBucketForLoginAndIP(login, ip string) error {
	_, err := t.grpcClient.ResetBuckets(t.ctx, &pb.ResetBucketsRequest{
		Login: login,
		Ip:    ip,
	})

	return err
}

func (t *apiTest) thereShouldBeOkResponsesAndNotOkResponses(wantOk, wantNotOk int) error {
	gotOk, gotNotOk := t.results[true], t.results[false]
	if gotOk != wantOk || t.results[false] != wantNotOk {
		return fmt.Errorf(
			"want %d ok and %d not ok, got %d ok and %d not ok", wantOk, wantNotOk, gotOk, gotNotOk,
		)
	}

	return nil
}

func (t *apiTest) sendRequests(requestsCount int, login, password, ip string) error {
	t.results = make(map[bool]int, requestsCount)
	loginFactory := getValueFactory(login)
	passwordFactory := getValueFactory(password)

	for i := 0; i < requestsCount; i++ {
		resp, err := t.grpcClient.CheckRequest(t.ctx, &pb.CheckRequestRequest{
			Login:    loginFactory(),
			Password: passwordFactory(),
			Ip:       ip,
		})
		if err != nil {
			return err
		}

		t.results[resp.Ok]++
	}

	return nil
}

func getValueFactory(value string) func() string {
	if value == "" {
		return func() string {
			return getRandomString(8)
		}
	}
	return func() string {
		return value
	}
}

func FeatureContext(s *godog.Suite) {
	t := &apiTest{
		ctx: context.Background(), // just stub
	}

	s.BeforeFeature(seedRandom)
	s.AfterFeature(t.closeConnection)

	s.Step(`^api running on host "([^"]*)" and port (\d+)$`, t.apiRunningOnHostAndPort)
	s.Step(`^I send (\d+) requests with same login "([^"]*)"$`, t.iSendRequestsWithSameLogin)
	s.Step(`^there should be (\d+) ok responses and (\d+) not ok responses$`, t.thereShouldBeOkResponsesAndNotOkResponses)
	s.Step(`^I send (\d+) requests with same password "([^"]*)" and random login$`, t.iSendRequestsWithSamePasswordAndRandomLogin)
	s.Step(`^I sent (\d+) requests with same ip "([^"]*)" and random login and password$`, t.iSentRequestsWithSameIpAndRandomLoginAndPassword)
	s.Step(`^I add subnet "([^"]*)" (\d+) into whitelist$`, t.iAddSubnetIntoWhitelist)
	s.Step(`^I add subnet "([^"]*)" (\d+) into blacklist$`, t.iAddSubnetIntoBlacklist)
	s.Step(`^I reset bucket for login "([^"]*)" and IP "([^"]*)"$`, t.iResetBucketForLoginAndIP)
}
