package cli

import (
	"context"
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"google.golang.org/grpc"

	"rate-limiter/internal/grpc/limiter"
)

// Cli struct contains grpc connection and client
// to reuse in subcommands
type Cli struct {
	ctx    context.Context
	conn   *grpc.ClientConn
	client limiter.AntiBruteForceServiceClient
}

func (c *Cli) preRunE(cmd *cobra.Command, args []string) error {
	host := cmd.Flags().Lookup("host").Value.String()
	port := cmd.Flags().Lookup("port").Value.String()
	address := host + ":" + port

	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		return fmt.Errorf("unable to connect: %w", err)
	}

	c.ctx = context.Background()
	c.conn = conn
	c.client = limiter.NewAntiBruteForceServiceClient(conn)

	return nil
}

func (c *Cli) postRun(cmd *cobra.Command, args []string) {
	_ = c.conn.Close() // have no idea what to do with error here
}

func init() {
	rootCmd.PersistentFlags().IntP("port", "p", 8000, "set limiter service port")
	rootCmd.PersistentFlags().String("host", "localhost", "set limiter hostname")

	rootCmd.AddCommand(bucketsCmd)
	rootCmd.AddCommand(whitelistsCmd, blacklistsCmd)
}

var rootCmd = &cobra.Command{
	Use:   "limiter-config",
	Short: "limiter-config is a cli for managing antibruteforce util",
	Long:  `limiter-config is a cli for managing antibruteforce util`,
	Run:   nil,
}

func Execute() {
	cli := &Cli{}

	rootCmd.PersistentPreRunE = cli.preRunE
	rootCmd.PersistentPostRun = cli.postRun

	bucketsCmd.RunE = cli.runBucketE

	whitelistsCmd.AddCommand(
		getAddCommand(cli.runAddToWhitelistE),
		getDeleteCommand(cli.runRemoveFromWhitelistE),
	)

	blacklistsCmd.AddCommand(
		getAddCommand(cli.runAddToBlacklistE),
		getDeleteCommand(cli.runRemoveFromBlacklistE),
	)

	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
