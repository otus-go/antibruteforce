package main

import (
	"context"
	"log"

	"github.com/kelseyhightower/envconfig"

	"rate-limiter/internal/grpc/limiter"
)

func main() {
	config := &limiter.Config{}

	err := envconfig.Process("limiter", config)
	if err != nil {
		panic(err)
	}

	log.Printf("sidecar address: %s", config.RateCounterAddress)

	ctx := context.Background()

	err = limiter.StartRateLimiterService(ctx, config)
	if err != nil {
		log.Fatalln("Unable to start service: ", err)
	}

	<-ctx.Done() // cuz StartCounterService is not blocking.
}
