package main

import (
	"context"
	"log"

	"github.com/kelseyhightower/envconfig"

	"rate-counter/internal/grpc/counter"
)

func main() {
	config := &counter.Config{}

	err := envconfig.Process("counter", config) // deliberately ignoring error
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	err = counter.StartCounterService(ctx, config)
	if err != nil {
		log.Fatalln("Unable to start service: ", err)
	}

	<-ctx.Done() // cuz StartCounterService is not blocking.
}
